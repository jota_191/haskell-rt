module Shapes (Shape,intersection,normal) where

import Vector
import Material             
import Ray
             
data Shape = Shape 
             { intersection :: Ray -> [Vector3D] 
             , normal:: Point3D -> Point3D
             }
             
             
sphere :: Point3D -> Double -> Shape
sphere center radius = Shape { intersection = \r -> [normalize $ gOrigin r]
                             , normal = \p -> p 
                             }
                       
---test
x = let center = point3D 0.0 0.0 0.0
        radius = 1.0
        origin = point3D (-2.0) (-3.0) 0.0
        dir    = vector3D 1.0 0.0 0.0
        r      = ray origin dir
        esfera = sphere center radius 
    in intersection esfera r
--
y = scale (sqrt 13) $ head x -- tiene que dar origin