module Material (Material,material,
                 gColor,gDiff,gSpec,gTrans,gRef) where

import Color

type KDiff    = Float --diffuse reflection index
type KSpec    = Float --specular reflection index 
type KTran    = Float --transmission coefficient
type RefIndex = Float --refraction index

data Material = Material Color KDiff KSpec KTran RefIndex
              deriving Show
                       
material = Material


gColor (Material c d s t r) = c
gDiff  (Material c d s t r) = d
gSpec  (Material c d s t r) = s
gTrans (Material c d s t r) = t
gRef   (Material c d s t r) = r