module Ray(ray,Ray,gOrigin,gDirection,translate) where

import Vector

data Ray = Ray Point3D Vector3D
            --origin --direction
           
--constructors 
ray o d = Ray o (normalize d)

--getters
gOrigin    (Ray o d) = o
gDirection (Ray o d) = d
--methods
translate (Ray o d) λ = o `plus` scale λ d
--TODO
--reflect (giving a normal)
