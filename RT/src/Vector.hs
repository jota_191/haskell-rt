module Vector (Vector3D, Point3D, vector3D, point3D, norm, (·), 
               normalize, plus,minus,scale,
               op) where

newtype Vector   = [Double]
type Vector3D = Vector
type Point3D  = Vector

vector3D :: Double -> Double -> Double -> Vector3D
point3D  :: Double -> Double -> Double -> Point3D
vector3D x y z = [x,y,z]
point3D x y z  = [x,y,z]

--getters
gX  = head
gY v= head$tail v
gZ v= head$tail$tail v

--methods
(·)      :: Vector -> Vector -> Double
norm     :: Vector -> Double
normalize:: Vector -> Vector
plus     :: Vector -> Vector -> Vector
minus    :: Vector -> Vector -> Vector
scale    :: Double -> Vector -> Vector
op       :: Vector -> Vector 
distance :: Vector -> Vector -> Double        
       
u · v        = sum $ zipWith (*) u v 
norm v       = sqrt $ v·v
normalize v  = map (/ norm v) v 
plus         = zipWith (+)
minus        = zipWith (-)
scale λ      = map (*λ)
op           = map (0-)
distance u v = norm $ u `minus` v
-------------------------------------------------------------------------------
--aux
-------------------------------------------------------------------------------
x = vector3D 1.0 2.0 1.0
y = vector3D 3.0 3.0 0.0

