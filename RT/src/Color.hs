module Color (Color,color,discreteColor,discretize,toCont,plus,minus,scale,
              black,white,red,lime,blue,yellow,cyan,magenta,silver,gray,maroon,
              olive,green,purple,teal,navy) where

type DiscreteColor = (Int, Int, Int)

type Color = (Float,Float,Float)

--constructors
color :: Float -> Float -> Float -> Color
color r g b = (r,g,b)

discreteColor :: Int -> Int -> Int -> DiscreteColor
discreteColor r g b = (r,g,b)
--getters
r (r,g,b) = r
g (r,g,b) = g
b (r,g,b) = b

--convertion
discretize :: Color ->  DiscreteColor 
discretize (r,g,b) = (round r, round g, round b)

toCont   :: DiscreteColor -> Color
toCont (r,g,b)   = (fromIntegral r, fromIntegral g, fromIntegral b)
  
plus  (r,g,b) (r',g',b') = (r+r',g+g',b+b')
minus (r,g,b) (r',g',b') = (r-r',g-g',b-b')     
scale (r,g,b)  λ         = (r*λ ,g*λ, b*λ )

black  = toCont (0,0,0)
white  = toCont (255,255,255)
red    = toCont (255,0,0)
lime   = toCont (0,255,0)
blue   = toCont (0,0,255)
yellow = toCont (255,255,0)
cyan   = toCont (0,255,255)
magenta= toCont (255,0,255)
silver = toCont (192,192,192)
gray   = toCont (128,128,128)
maroon = toCont (128,0,0)
olive  = toCont (128,128,0)
green  = toCont (0,128,0)
purple = toCont (128,0,128)
teal   = toCont (0,128,128)
navy   = toCont (0,0,128)